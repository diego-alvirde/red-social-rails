Rails.application.routes.draw do
  # resources :publicacions
  resources :publicaciones do 
    resources :comentarios
  end
  resources :usuarios  

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/login' , to: "login#new"
  post '/login', to: "login#create"
  delete '/logout', to: "login#destroy"

  get '/usuarios/:id/muro', to: "usuarios#muro", as: :muro_usuario 

  root "login#new"
end
