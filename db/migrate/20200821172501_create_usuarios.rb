class CreateUsuarios < ActiveRecord::Migration[6.0]
  def change
    create_table :usuarios do |t|
      t.string :nombre
      t.string :paterno
      t.string :materno
      t.string :rfc
      t.integer :clav

      t.timestamps
    end
  end
end
