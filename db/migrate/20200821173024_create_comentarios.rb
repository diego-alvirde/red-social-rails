class CreateComentarios < ActiveRecord::Migration[6.0]
  def change
    create_table :comentarios do |t|
      t.string :contenido, limit: 100
      t.references :usuario, null: false, foreign_key: true
      t.references :publicacion, null: false, foreign_key: true

      t.timestamps
    end
  end
end
