class CreatePublicaciones < ActiveRecord::Migration[6.0]
  def change
    create_table :publicaciones do |t|
      t.string :titulo
      t.text :contenido
      t.references :usuario, null: false, foreign_key: true

      t.timestamps
    end
  end
end
