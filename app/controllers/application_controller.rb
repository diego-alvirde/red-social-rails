class ApplicationController < ActionController::Base
  before_action :sesion
  def sesion
    redirect_to login_path if session[:id].nil?
  end 
end
