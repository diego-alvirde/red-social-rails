class ComentariosController < ApplicationController
  def create
    comentario = Comentario.new
    comentario.contenido = params[:comentario][:contenido]
    comentario.usuario_id = session[:id]
    comentario.publicacion_id = params[:publicacion_id]    
    comentario.save

    redirect_to muro_usuario_path(session[:id])
  end 
end
