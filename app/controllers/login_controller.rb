class LoginController < ApplicationController
  skip_before_action :sesion
  def new
  end

  def create
    usuario = params[:usuario]
    password = params[:password]
    usuario = Usuario.where("nombre = ? and paterno = ?", usuario,password).first
    if usuario.nil?
      flash.now[:error] = "Usuario o password incorrectos"
      render :new
    else
      session[:id] = usuario.id
      session[:nombre] = usuario.nombre
      redirect_to muro_usuario_path(usuario)
    end
  end

  def destroy
    session[:id] = nil
    session[:nombre] = nil
    redirect_to login_path
  end
end
